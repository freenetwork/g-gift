var mongoose = require('mongoose')
var bcrypt   = require('bcrypt-nodejs');

var accountSchema = mongoose.Schema({
    url: String,
    name: String,
    photo: String,
    provider: String,
    balance: Number,
    bonus: Number,
    hash_balance: String,
    hash_bonus: String,
    updated_at: String
});

accountSchema.pre('save', function(next) {
    var currentDate = Date.now() / 1000 | 0
	this.updated_at = currentDate;
	if (!this.created_at)
		this.created_at = currentDate;
    this.hash_balance = this.generateHash(this.balance)
    this.hash_bonus = this.generateHash(this.bonus)
	next();
});

accountSchema.pre('update', function(next) {
    var currentDate = Date.now() / 1000 | 0
    this.updated_at = currentDate;
    if (!this.created_at)
        this.created_at = currentDate;
    this.hash_balance = this.generateHash(this.balance)
    this.hash_bonus = this.generateHash(this.bonus)
    next();
});

accountSchema.methods.generateHash = function(string) {
    return bcrypt.hashSync(string, bcrypt.genSaltSync(8), null);
};

accountSchema.methods.validBalance = function(balance) {
    return bcrypt.compareSync(balance, this.hash_balance);
};

accountSchema.methods.validBonus = function(bonus) {
    return bcrypt.compareSync(bonus, this.hash_bonus);
};

module.exports = mongoose.model('Account', accountSchema);