var mongoose = require('mongoose')
var bcrypt   = require('bcrypt-nodejs');

var gameSchema = mongoose.Schema({
    name: String, //наименование 
    image: String, //изображения разыгрываемого предмета/игры/подарка
    players: Object, //список участников
    places: Number, //количество мест в игре
    price: Number, //цена на рынке
    tiket: Number, //стоимость участия в игре
    type: Number, // 0 - cs:go, 1 - dota2, 3 - подарочные исполнения игр, 4 - реальные предметы
    state: Number, // 0 - создано но не разыгрывается, 1 - игра в процесее, 2 - игра завершена
    busy: Number, // количество занятых мест
    hash_ticket: String, //hash на стоимость участия
    updated_at: String //время создания/обновления игры
});

gameSchema.pre('save', function(next) {
    var currentDate = Date.now() / 1000 | 0
	this.updated_at = currentDate;
	if (!this.created_at)
		this.created_at = currentDate;
    this.hash_ticket = this.generateHash(this.tiket)
	next();
});

gameSchema.methods.generateHash = function(tiket) {
    return bcrypt.hashSync(tiket, bcrypt.genSaltSync(8), null);
};

gameSchema.methods.validTiket= function(tiket) {
    return bcrypt.compareSync(tiket, this.hash_tiket);
};

module.exports = mongoose.model('Game', gameSchema);