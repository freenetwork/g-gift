var mongoose = require('mongoose')
var bcrypt   = require('bcrypt-nodejs');

var orderSchema = mongoose.Schema({
    ik_co_id: String,
    ik_co_prs_id: Number,
    ik_inv_id: Number,
    ik_inv_st: String,
    ik_inv_crt: String,
    ik_inv_prc: String,
    ik_trn_id: String,
    ik_pm_no: String,
    ik_pw_via: String,
    ik_am: Number,
    ik_co_rfn: Number,
    ik_ps_price: Number,
    ik_cur: String,
    ik_x_username: String,
    ik_x_provider: String,
    ik_x_url: String,
    ik_sign: String,
    created_at: String
});

orderSchema.pre('save', function(next) {
    var currentDate = Date.now() / 1000 | 0
	this.updated_at = currentDate;
	if (!this.created_at)
		this.created_at = currentDate;
	next();
});

module.exports = mongoose.model('Order', orderSchema);