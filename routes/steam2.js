var Winston = require('winston'); // Для логирования
var SteamUser = require('steam-user'); // Для всех операций, steam api
var TradeOfferManager = require('steam-tradeoffer-manager'); // Для покупки продажи товаров
var config = require('./config.js');
var fs = require('fs')

// Индификаторы приложений
var appid = {
	TF2: 440,
	DOTA2: 570,
	CSGO: 730,
	Steam: 753
};

// Контекст это индификатор объекта, для Steam 2 - подарки, 6 карты, эмоции, заставки
// Для все настоящих игр Valve контекст равен 2
var contextid = {
	TF2: 2,
	DOTA2: 2,
	CSGO: 2,
	Steam: 6
}

// Настройки для логирования в консоль и в файл
var logger = new(Winston.Logger)({
	transports: [
		new(Winston.transports.Console)({
			colorize: true,
			level: 'debug',
			timestamp: true
		}),
		new(Winston.transports.File)({
			level: 'info',
			timestamp: true,
			filename: 'applog.log',
			json: true
		})
	]
});

// Инициализация клиента и торгового бота
var client = new SteamUser();
var offers = new TradeOfferManager({
	steam: client,
	domain: config.domain,
	language: "en", // Язык описания товара
	pollInterval: 10000, // (Poll every 10 seconds (10,000 ms)
	cancelTime: 300000, // Expire any outgoing trade offers that have been up for 5+ minutes (300,000 ms)
	apiKey: 'FB3C14A3388E52993B198D8CF20A04DE'
});

console.log(SteamUser)

fs.readFile('polldata.json', function(err, data) {
	if (err) {
		logger.warn('Ошибка чтения polldata.json. Если это первый запуск то не обращайте внимание на эту ошибку: ' + err);
	} else {
		logger.debug("Найден файл с предыдущей историей торговли, начинаю его импорт для продолжения торговли.");
		offers.pollData = JSON.parse(data);
	}
});

// Вход в Steam
client.logOn({
	accountName: config.username,
	password: config.password
}, function(err, data) {
	console.log(data);
});

client.on('loggedOn', function(response) {
	logger.info("Выполнен вход в Steam как " + response.vanity_url + ' его индификатор '+ client.steamID);
});

client.on('error', function(e) {
	// Большинство ошибок сопровождается выходом 
	// https://github.com/SteamRE/SteamKit/blob/SteamKit_1.6.3/Resources/SteamLanguage/eresult.steamd
	logger.error("Произошла ошибка " + e);
	process.exit(1);
});

client.on('webSession', function(sessionID, cookies) {
	logger.debug("Установка web сессии. Установка статуса Online");
	// client.friends.setPersonaState(SteamUser.Steam.EPersonaState.Online);
	offers.setCookies(cookies, function(err) {
		if (err) {
			logger.error('Невозможно установить cookie для продолжения торговли: ' + err);
			process.exit(1);
		}
		logger.debug("Cookie установлены.  Получен API Key: " + offers.apiKey);
	});
});

client.on('newItems', function (count) {
	logger.info(count + " новых предметов в нашем инвентаре");
});

client.on('wallet', function(hasWallet, currency, balance) {
	if (hasWallet) {
		logger.info("Денежные средства на счету: " + SteamUser.formatCurrency(balance, currency));
		//io.emit('message', SteamUser.formatCurrency(balance, currency));
	} else {
		logger.info("Денежные средства на счету отсутствуют");
	}
});

client.on('emailInfo', function(address, validated) {
	logger.info("Ваш почта " + address + " . Она " + (validated ? "подтверждена" : "не подтверждена"));
});

client.on('friendsList', function(res) {
	console.log(res)
})

// client.friends.on('relationships', function() {
// 	var friendcount = 0;
// 	// В цикле для каждого друга мы можем сделать операцию.
// 	for (steamID in client.friends.friends) {
// 		friendcount++;
// 		if (client.friends.friends[steamID] === SteamUser.Steam.EFriendRelationship.RequestRecipient) {
// 			logger.info("Друзья которые предлогали дружбу пока Вы были офлайн: " + steamID);
// 			client.friends.addFriend(steamID);
// 		}
// 	}
// 	console.log(client.friends.friends);
// 	logger.debug("У Вас " + friendcount + " друзей.");
// 	if (friendcount > 200) {
// 		logger.warn("У Вас превышенно количество друзей.  Почистите старых друзей");
// 	}
// });

// client.friends.on('friend', function(steamID, relationship) {
// 	if (relationship == SteamUser.Steam.EFriendRelationship.RequestRecipient) {
// 		logger.info('Пользователь с SteamId [' + steamID + '] запрашивает дружбу');
// 		client.friends.addFriend(steamID);
// 	} else if (relationship == SteamUser.Steam.EFriendRelationship.None) {
// 		logger.info('Пользователь с SteamId [' + steamID + '] Вам больше не друг.');
// 	}
// });

client.on('accountLimitations', function(limited, communityBanned, locked, canInviteFriends) {
	if (limited) {
		logger.warn("Ваш аккаунт ограничен. Вы не можете приглашать друзей, использовать магазин, открывать груповой чат, и пользоваться Steam Web API");
	}
	if (communityBanned) {
		logger.warn("Вы имеете бан от Steam Community");
	}
	if (locked) {
		logger.error("Ваш аккаунт заблокирован. Вы не можете передавать, получать и покупать подарки предметы и прочее, играть на VAC серверах, выполняю завершение рпограммы.");
		process.exit(1);
	}
	if (!canInviteFriends) {
		logger.warn("Вы не можете предлогать дружбу");
	}
});