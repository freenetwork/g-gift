var RandomOrg = require('random-org');

module.exports = function(app, passport) {
    app.get('/', function(req, res, next) {
        res.render('index', {
            title: 'Express',
            user: req.user
        });
    });

    app.get('/auth/steam',
        passport.authenticate('steam', {
            failureRedirect: '/'
        }),
        function(req, res) {
            res.redirect('/');
        });

    app.get('/auth/steam/success',
        passport.authenticate('steam', {
            failureRedirect: '/'
        }),
        function(req, res) {
            res.redirect('/');
        });

    app.get('/auth/vk',
        passport.authenticate('vkontakte', {
            failureRedirect: '/'
        }),
        function(req, res) {
            res.redirect('/');
        });

    app.get('/auth/vk/success',
        passport.authenticate('vkontakte', {
            failureRedirect: '/'
        }),
        function(req, res) {
            res.redirect('/');
        });

    app.get('/auth/google',
        passport.authenticate('google', {
            failureRedirect: '/'
        }),
        function(req, res) {
            res.redirect('/');
        });

    app.get('/auth/google/success',
        passport.authenticate('google', {
            failureRedirect: '/'
        }),
        function(req, res) {
            res.redirect('/');
        });

    app.get('/auth/facebook',
        passport.authenticate('facebook', {
            failureRedirect: '/'
        }),
        function(req, res) {
            res.redirect('/');
        });

    app.get('/auth/facebook/success',
        passport.authenticate('facebook', {
            failureRedirect: '/'
        }),
        function(req, res) {
            res.redirect('/');
        });

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/random', function(req, res) {
        var random = new RandomOrg({
            apiKey: '112eaf6e-fba2-4749-882d-a34ae3566c84'
        });
        random.generateSignedIntegers({
                min: 1,
                max: 99,
                n: 1
            })
            .then(function(result) {
                console.log(result.random);
                console.log(result.signature);

				res.render('random', {
					data: result.random,
					signature: result.signature
				});
            });
    })

}

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}