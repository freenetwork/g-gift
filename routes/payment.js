var co = require('co')
var _Order = require('../models/order')
var _User = require('../models/account')

module.exports = function(app, passport) {

    app.post('/pay/success', function(req, res) {
        // console.log('success')
        // console.log(req.body)
        res.redirect('/');
    });

    app.post('/pay/failure', function(req, res) {
        // console.log('failure')
        // console.log(req.body)
        res.redirect('/');
    });

    app.post('/pay/wait', function(req, res) {
        // console.log('wait')
        // console.log(req.body)
        res.redirect('/');
    });

    app.post('/pay/processing', function(req, res, next) {
        console.log(req.body)

        var _user = co.wrap(function*() {
            var data = yield _User.count({
                name: req.body.ik_x_username,
                url: req.body.ik_x_url,
                provider: req.body.ik_x_provider
            }).exec()
            return data;
        });

        var user = co.wrap(function*() {
            var data = yield _User.findOne({
                name: req.body.ik_x_username,
                url: req.body.ik_x_url,
                provider: req.body.ik_x_provider
            }).exec()
            return data;
        });

        var _result = co.wrap(function*() {
            var data = yield _Order.count({
                ik_co_id: req.body.ik_co_id,
                ik_co_prs_id: req.body.ik_co_prs_id,
                ik_inv_id: req.body.ik_inv_id,
                ik_inv_st: req.body.ik_inv_st,
                ik_inv_crt: req.body.ik_inv_crt,
                ik_inv_prc: req.body.ik_inv_prc,
                ik_trn_id: req.body.ik_trn_id,
                ik_pm_no: req.body.ik_pm_no,
                ik_pw_via: req.body.ik_pw_via,
                ik_am: req.body.ik_am,
                ik_co_rfn: req.body.ik_co_rfn,
                ik_ps_price: req.body.ik_ps_price,
                ik_cur: req.body.ik_cur,
                ik_x_username: req.body.ik_x_username,
                ik_x_provider: req.body.ik_x_provider,
                ik_x_url: req.body.ik_x_url,
                ik_sign: req.body.ik_sign,
            }).exec()
            return data;
        });

        _user().then(function(data) {
            if(data > 0){
                user().then(function(data) {

                });
            }
            else{
                console.log('Такого пользователя не существует. Платеж не будет зачислен на баланс.')
                res.status(201)
            }

        });

        _user().catch(function(err) {
            console.log(err)
            throw err
        })
    });




}

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}