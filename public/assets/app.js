$(function() {
	function stringifyTomorrow() {
		var today = moment();
		var tomorrow = today.add('days', 1);
		return moment(tomorrow).format("YYYY-MM-DD");
	}
	$('#payment-expire').val(stringifyTomorrow)
	$('#payment-number').val(stringifyTomorrow)
});