var SteamStrategy = require('passport-steam').Strategy;
module.exports = function(passport) {
	passport.use(new SteamStrategy({
				returnURL: 'http://127.0.0.1:3000/auth/steam/success',
				realm: 'http://127.0.0.1:3000/',
				apiKey: '1B4872FF7D6EF2A963D4142B7E04D682'
			},
			function(identifier, profile, done) {
				console.log("steam auth: ", profile);
				return done(null, {
					name: profile.displayName,
					photo: profile.photos[2].value,
					url: profile.identifier,
					provider: profile.provider
				});
			}
		)
	);
}