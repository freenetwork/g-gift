_User = require('../models/account')
module.exports = function(passport) {
	passport.serializeUser(function(user, done) {
		//console.log(user)
		done(null, user);
	});

	passport.deserializeUser(function(obj, done) {
		//console.log(obj)
		_User.findOne({name: obj.name, url: obj.url, provider: obj.provider}, function(err, user){
			if(err)
				console.log(err)
			done(null, user);
		})
	});
}