var _User = require('../models/account');
var co = require('co')
var VKStrategy = require('passport-vkontakte').Strategy;
module.exports = function(passport) {
	passport.use(new VKStrategy({
				callbackURL: 'http://speeddatingkrsk.ru/auth/vk/success',
				clientSecret: 'c7HBpnqYsdpUYK4VQyr3',
				clientID: '5385036'
			},
			function (accessToken, refreshToken, profile, done) {
				console.log("vk auth: ", profile);

				var result = co.wrap(function*() {
					var data = yield _User.count({
						name: profile.displayName,
						photo: profile.photos[0].value,
						url: profile.profileUrl,
						provider: profile.provider
					}).exec()
					return data;
				});


				 result().then(function(data) {

					if (data > 0) {
						_User.findOne({name: profile.displayName, photo: profile.photos[0].value, url: profile.profileUrl, provider: profile.provider}, function(err, result) {
							if (err) {
								throw err
							}
							else{
								return done(null, result)
							}
						})
						
						 // return done(null, {
						// 	name: data.displayName,
						// 	photo: data.photos[0].value,
						// 	url: data.profileUrl,
						// 	provider: data.provider,
						// 	balance: data.balance,
						// 	hash_balance: data.hash_balance
						// });
					}
					else {
						var user = new _User({
							name: profile.displayName,
							photo: profile.photos[0].value,
							url: profile.profileUrl,
							provider: profile.provider,
							balance: 5000,
							bonus: 50
						})

						user.save(function(err, result) {
							if (err) {
								throw err
							}
							else{
								return done(null, result)
								// {
								// 	name: profile.displayName,
								// 	photo: profile.photos[0].value,
								// 	url: profile.profileUrl,
								// 	provider: profile.provider
								// });
							}
						})
					}

				});

				result().catch(function(err) {
					console.log(err)
				})

				// return done(null, {
				// 	name: profile.displayName,
				// 	photo: profile.photos[0].value,
				// 	url: profile.profileUrl,
				// 	provider: profile.provider
				// });
			}
		)
	);
}