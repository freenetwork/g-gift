var FacebookStrategy = require('passport-facebook').Strategy;
module.exports = function(passport) {
	passport.use(new FacebookStrategy({
	    clientID: '1748048115481738',
	    clientSecret: '74de1567f75cd4836ff0e1ca88ff8e8f',
	    callbackURL: "http://127.0.0.1:3000/auth/facebook/success",
	    profileFields: ['id', 'displayName', 'picture', 'email', 'name', 'profileUrl', 'gender']
	  },
	  function(accessToken, refreshToken, profile, done) {
	    console.log("fb auth: ", profile);
		return done(null, {
			name: profile.displayName,
			photo: profile.photos[0].value,
			url: profile.profileUrl,
			provider: profile.provider
		});
	  }
	));
}