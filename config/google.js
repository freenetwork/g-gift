var GoogleStrategy = require( 'passport-google-oauth2' ).Strategy;
module.exports = function(passport) {
  passport.use(new GoogleStrategy({
      clientID:     '1054048421223-1eg753ki90arc0r8m2cqhfpc7d6ghljj.apps.googleusercontent.com',
      clientSecret: '-Y1qofAMvtRbuU05KPxdfDNx',
      callbackURL: "http://127.0.0.1:3000/auth/google/success",
      passReqToCallback   : true,
      scope: 'https://www.googleapis.com/auth/plus.login'
    },
    function(request, accessToken, refreshToken, profile, done) {
        console.log("google auth: ", profile);
        return done(null, {
          name: profile.displayName,
          photo: profile.photos[0].value,
          url: profile.profileUrl,
          provider: profile.provider
        });
    }
  ));
}